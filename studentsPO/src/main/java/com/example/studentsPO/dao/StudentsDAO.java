package com.example.studentsPO.dao;

import com.example.studentsPO.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentsDAO extends JpaRepository<Student, Integer> {

}
