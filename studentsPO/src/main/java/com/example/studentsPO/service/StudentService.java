package com.example.studentsPO.service;

import com.example.studentsPO.entity.Student;

import java.util.List;
import java.util.Optional;

public interface StudentService {

    Student insert(Student student);

    Optional<Student> getOne(int id);

    List<Student> getAll();

    Student update(int id, Student updatedStudent);

    void delete(int id);

    int getLastIndex();

}
