package com.example.studentsPO.service;

import com.example.studentsPO.dao.StudentsDAO;
import com.example.studentsPO.entity.Student;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class StudentServiceImplementation implements StudentService {

    private final StudentsDAO studentsDAO;

    public StudentServiceImplementation(StudentsDAO studentsDAO) {
        this.studentsDAO = studentsDAO;
    }

    @Override
    public Student insert(Student student) {
        return studentsDAO.save(student);
    }

    @Override
    public Optional<Student> getOne(int id) {
        return studentsDAO.findById(id);
    }

    @Override
    public List<Student> getAll() {
        return studentsDAO.findAll();
    }

    @Override
    public Student update(int id, Student updatedStudent) {
        return studentsDAO.save(updatedStudent);
    }

    @Override
    public void delete(int id) {
        studentsDAO.deleteById(id);
    }

    @Override
    public int getLastIndex() {
        return studentsDAO.findAll(Sort.by(Sort.Direction.DESC, "index")).get(0).getIndex();
    }

}
