package com.example.studentsPO.controller;

import com.example.studentsPO.entity.Student;
import com.example.studentsPO.service.StudentService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/students")
public class StudentsController {
    private final StudentService studentService;

    public StudentsController(StudentService studentService) {
        this.studentService = studentService;
    }


    @GetMapping("/get/{id}")
    public Student get(@PathVariable int id) {
        return studentService.getOne(id).orElse(null);
    }

    @GetMapping("/get")
    public List<Student> get() {
        return studentService.getAll();
    }

    @PostMapping("/insert")
    public Student insert(@RequestBody Student student) {
        if (student.getIndex() == 0) {
            student.setIndex(studentService.getLastIndex() + 1);
        }
        return studentService.insert(student);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<?> update(@RequestBody Student updatedStudent, @PathVariable int id) {
        if (updatedStudent.getId() != id) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("ID parametru niezgadza się z ID studenta");
        }
        return ResponseEntity.status(HttpStatus.OK).body(studentService.update(id, updatedStudent));
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "ID studenta niezgodne z ID parametru")
    public void badID() {
    }

    @DeleteMapping("/delete/{id}")
    void removeStudent(@PathVariable int id) {
        studentService.delete(id);
    }

}
