package com.example.studentsPO;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StudentsPoApplication {

	public static void main(String[] args) {
		SpringApplication.run(StudentsPoApplication.class, args);
	}

}
