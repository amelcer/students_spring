package app;

import app.model.Student;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import org.json.JSONObject;

public class Helpers {

    public static void acceptOnlyDigits(TextField input) {
        input.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("\\d*")) {
                input.setText(newValue.replaceAll("[^\\d]", ""));
            }
        });
    }

    public static void testDateFormat(String input) throws DateFormatException {
        String datePattern = "([12]\\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\\d|3[01]))";
        if (!input.matches(datePattern)) {
            throw new DateFormatException("Data nie pasuje do formatu YYYY-MM-DD");
        }
    }

    public static void alert(Alert.AlertType type, String title, String message) {
        Alert alert = new Alert(type);
        alert.setTitle(title);
        alert.setContentText(message);

        alert.showAndWait();
    }

    public static Student getStudentFromJSON(JSONObject json) {
        if (json.has("id"))
            return new Student(
                    json.getInt("id"),
                    json.getString("name"),
                    json.getString("lastName"),
                    json.getString("birthDay"),
                    json.getString("department"),
                    json.getInt("index")
            );
        else
            return new Student(
                    json.getString("name"),
                    json.getString("lastName"),
                    json.getString("birthDay"),
                    json.getString("department"),
                    json.getInt("index")
            );
    }
}
