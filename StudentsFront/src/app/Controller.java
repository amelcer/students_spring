package app;


import app.api.API;
import app.model.Student;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.paint.Color;
import javafx.util.Callback;
import javafx.util.converter.IntegerStringConverter;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;


public class Controller {
    @FXML
    TextField index;
    @FXML
    TextField studentName;
    @FXML
    TextField lastName;
    @FXML
    TextField birthDate;
    @FXML
    ComboBox<String> department;
    @FXML
    CheckBox autoIndex;

    private Control[] formInputs;

    @FXML
    TableView<Student> studentsTable;
    @FXML
    TableColumn<Student, Integer> colID;
    @FXML
    TableColumn<Student, Integer> colIndex;
    @FXML
    TableColumn<Student, String> colName;
    @FXML
    TableColumn<Student, String> colLastName;
    @FXML
    TableColumn<Student, String> colBirthDay;
    @FXML
    TableColumn<Student, String> colDepartment;
    @FXML
    TableColumn colActions;

    List<TableColumn> columns;

    @FXML
    Label infoLabel;

    @FXML
    public void addNewStudent() {
        try {
            resetCss();

            String name = studentName.getText().trim();
            String lName = lastName.getText().trim();
            String bDay = birthDate.getText().trim();
            Helpers.testDateFormat(bDay);
            String dep = department.getSelectionModel().getSelectedItem();

            Student newStudent = null;
            if (autoIndex.isSelected()) {
                newStudent = new Student(name, lName, bDay, dep, 0);
            } else {
                int ind = Integer.parseInt(index.getText());
                newStudent = new Student(name, lName, bDay, dep, ind);
            }

            if (newStudent != null) {
                API.addNewStudent(newStudent);
                loadTableData();
                setInfoLabel(true, "Dodano");
            }

        } catch (DateFormatException e) {
            birthDate.getStyleClass().add("error");
            Helpers.alert(Alert.AlertType.ERROR, "Błędny format daty", e.getMessage());
        } catch (NumberFormatException e) {
            index.getStyleClass().add("error");
            Helpers.alert(Alert.AlertType.ERROR, "Nieprawidłowy index", e.getMessage());
        }

    }

    @FXML
    void clearForm() {
        for (Control input : formInputs) {
            String type = input.getClass().getSimpleName();
            if (type.equals("TextField")) {
                var textInput = (TextField) input;
                textInput.setText("");
            } else if (type.equals("ComboBox")) {
                var box = (ComboBox<String>) input;
                box.getSelectionModel().select(0);
            }
        }
        autoIndex.setSelected(true);
        setIndexInput();
        resetCss();
    }

    @FXML
    void setIndexInput() {
        index.setDisable(autoIndex.isSelected());
        index.setText("");
    }


    @FXML
    void initialize() {
        formInputs = new Control[]{index, studentName, lastName, birthDate, department};
        columns = new ArrayList<>();
        columns.add(colID);
        columns.add(colIndex);
        columns.add(colName);
        columns.add(colLastName);
        columns.add(colBirthDay);
        columns.add(colDepartment);

        department.getItems().addAll("WE", "WN", "WM", "WPIT");
        Helpers.acceptOnlyDigits(index);

        initTable();
        loadTableData();
    }

    private void resetCss() {
        for (Control input : formInputs) {
            input.getStyleClass().removeIf(style -> style.equals("error"));
        }
    }

    private void loadTableData() {
        ObservableList<Student> students = FXCollections.observableArrayList();
        students.addAll(API.getStudents());
        studentsTable.setItems(students);
    }

    private void initTable() {
        String[] colNames = {"id", "index", "name", "lastName", "birthDay", "department"};
        IntStream.range(0, columns.size()).forEach(idx -> {
            columns.get(idx).setCellValueFactory(new PropertyValueFactory<>(colNames[idx]));
        });

        addDeleteButtonToTable();

        setEditableColumns();
    }

    private void addDeleteButtonToTable() {
        Callback<TableColumn<Student, String>, TableCell<Student, String>> cellFactory = new Callback<>() {
            @Override
            public TableCell call(final TableColumn<Student, String> param) {
                final TableCell<Student, String> cell = new TableCell<>() {

                    final Button btn = new Button("Usuń");

                    @Override
                    public void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            btn.setOnAction(event -> {
                                Student student = getTableView().getItems().get(getIndex());
                                removeStudent(student);
                            });
                            setGraphic(btn);
                        }
                    }
                };
                return cell;
            }
        };

        colActions.setCellFactory(cellFactory);
    }

    private void setEditableColumns() {

        colIndex.setCellFactory(TextFieldTableCell.<Student, Integer>forTableColumn(new IntegerStringConverter()));
        colIndex.setOnEditCommit(e -> {
            Student tmp = e.getTableView().getItems().get(e.getTablePosition().getRow());
            tmp.setIndex(e.getNewValue());
            updateStudent(tmp);
        });

        colName.setCellFactory(TextFieldTableCell.forTableColumn());
        colName.setOnEditCommit(e -> {
            Student tmp = e.getTableView().getItems().get(e.getTablePosition().getRow());
            tmp.setName(e.getNewValue());
            updateStudent(tmp);
        });

        colLastName.setCellFactory(TextFieldTableCell.forTableColumn());
        colLastName.setOnEditCommit(e -> {
            Student tmp = e.getTableView().getItems().get(e.getTablePosition().getRow());
            tmp.setLastName(e.getNewValue());
            updateStudent(tmp);
        });

        colBirthDay.setCellFactory(TextFieldTableCell.forTableColumn());
        colBirthDay.setOnEditCommit(e -> {
            Student tmp = e.getTableView().getItems().get(e.getTablePosition().getRow());
            try {
                studentsTable.refresh();
                Helpers.testDateFormat(e.getNewValue().toString());
                tmp.setBirthDay(e.getNewValue());
                updateStudent(tmp);
            } catch (DateFormatException dateFormatException) {
                Helpers.alert(Alert.AlertType.ERROR, "Błędny format daty", dateFormatException.getMessage());
            }
        });

        colDepartment.setCellFactory(TextFieldTableCell.forTableColumn());
        colDepartment.setOnEditCommit(e -> {
            Student tmp = e.getTableView().getItems().get(e.getTablePosition().getRow());
            tmp.setDepartment(e.getNewValue());
            updateStudent(tmp);
        });

        studentsTable.setEditable(true);
    }

    private void updateStudent(Student student) {
        if (API.updateStudent(student)) {
            setInfoLabel(true, "Pomyślnie zaktualizowano");
        } else {
            setInfoLabel(false, "Problem z aktualizacją studenta");
        }
    }

    private void removeStudent(Student student) {
        if (API.removeStudent(student)) {
            studentsTable.getItems().remove(student);
            setInfoLabel(true, "Usunięto");
        } else {
            setInfoLabel(false, "Problem z usunięciem studenta");
        }
    }

    private void setInfoLabel(Boolean success, String message) {
        if (success) {
            infoLabel.setTextFill(Color.web("#000000"));
        } else {
            infoLabel.setTextFill(Color.web("#ff0000"));
        }
        infoLabel.setText(message);
    }

}
