package app.api;

import app.Helpers;
import app.model.Student;
import javafx.scene.control.Alert;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.List;

public class API {
    private final static String API_URL = "http://localhost:8080/students";

    public static List<Student> getStudents() {
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder(URI.create(API_URL + "/get")).build();
        try {
            var response = client.send(request, HttpResponse.BodyHandlers.ofString());
            if (response.statusCode() != 200) {
                showInfoAlert(response.statusCode());
            }
            return getStudentsFromJsonResponse(response.body());
        } catch (InterruptedException | IOException e) {
            e.printStackTrace();
        }
        return new ArrayList<Student>();
    }

    private static List<Student> getStudentsFromJsonResponse(String responseBody) {
        JSONArray response = new JSONArray(responseBody);
        List<Student> list = new ArrayList<>();

        for (int i = 0; i < response.length(); i++) {
            list.add(Helpers.getStudentFromJSON(response.getJSONObject(i)));
        }
        return list;
    }

    public static void addNewStudent(Student student) {
        JSONObject JStudent = new JSONObject(student);
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder(URI.create(API_URL + "/insert"))
                .header("Accept", "application/json")
                .header("Content-type", "application/json")
                .POST(HttpRequest.BodyPublishers.ofString(JStudent.toString()))
                .build();
        try {
            var response = client.send(request, HttpResponse.BodyHandlers.ofString());
            if (response.statusCode() != 200) {
                showInfoAlert(response.statusCode());
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static boolean removeStudent(Student student) {
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder(URI.create(API_URL + "/delete/" + student.getId()))
                .header("Accept", "application/json")
                .DELETE()
                .build();
        try {
            var response = client.send(request, HttpResponse.BodyHandlers.ofString());
            if (response.statusCode() != 200) {
                showInfoAlert(response.statusCode());
                return false;
            }
        } catch (InterruptedException | IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public static boolean updateStudent(Student student) {
        JSONObject JStudent = new JSONObject(student);
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder(URI.create(API_URL + "/update/" + student.getId()))
                .header("Accept", "application/json")
                .header("Content-type", "application/json")
                .PUT(HttpRequest.BodyPublishers.ofString(JStudent.toString()))
                .build();
        try {
            var response = client.send(request, HttpResponse.BodyHandlers.ofString());
            if (response.statusCode() != 200) {
                showInfoAlert(response.body(), response.statusCode());
                return false;
            }
        } catch (InterruptedException | IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    private static void showInfoAlert(int statCode) {
        Helpers.alert(Alert.AlertType.INFORMATION, "Bład po stronie serwera", "Kod błędu: " + statCode);
    }

    private static void showInfoAlert(String message, int statCode) {
        Helpers.alert(Alert.AlertType.INFORMATION, "Bład po stronie serwera", message + " \n Kod błędu: " + statCode);
    }

}
