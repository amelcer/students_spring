package app.model;

public class Student extends Person {
    private int index;
    private String department;


    public Student(String name, String lastName, String birthDay, String department, int index) {
        super(name, lastName, birthDay);
        this.department = department;
        this.index = index;
    }

    public Student(int id, String name, String lastName, String birthDay, String department, int index) {
        super(id, name, lastName, birthDay);
        this.department = department;
        this.index = index;

    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

}
